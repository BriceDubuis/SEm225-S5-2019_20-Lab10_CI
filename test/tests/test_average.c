#include "average.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>
#include "stdint.h"


void test_sum_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(0, 1), "Error in integer_avg");
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0,-1), "Error in integer_avg");
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(255,-255), "Error in integer_avg");
	TEST_ASSERT_EQUAL_INT_MESSAGE(24, integer_avg(0,48), "Error in integer_avg");
	
}