
#include "average.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>
#include "stdint.h"


int32_t integer_avg(int32_t p1, int32_t p2){
	return int32_t((p1+p2)/2);
}
