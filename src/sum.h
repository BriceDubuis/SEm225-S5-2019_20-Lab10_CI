#ifndef __SUM_H
#define __SUM_H

#include "stdint.h"
int integer_sum(int a, int b);
int32_t integer_sum_32(int a, int b);
#endif //__SUM_H
