#include "sum.h"

/* Function that return the sum of two integers */
int integer_sum(int a, int b)
{
    return a + b;
}

int32_t integer_sum_32(int a, int b)
{
	return a + b;
}